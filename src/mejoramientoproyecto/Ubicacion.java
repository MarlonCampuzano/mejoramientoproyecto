/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mejoramientoproyecto;

/**
 *
 * @author usuario
 */
public class Ubicacion {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Ubicacion(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    static double calcularDistancia(Ubicacion u1, Ubicacion u2){
        double x1 = u1.getX();
        double x2 = u1.getY();
        double y1 = u2.getX();
        double y2 = u2.getY();
        double r1 = Math.pow(x2-x1,2);
        double r2 = Math.pow(y2-y1,2);
        return Math.sqrt(r1+r2);
    }
    
    
}
