/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mejoramientoproyecto;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author IKAROS
 */
public class CreacionAgente extends Application{
    
    Label lblnombre = new Label("Nombre");
    Label lblcedula = new Label("cedula");
    Label lblruta = new Label("Ruta imagen");
    Label lblX = new Label("Ubicacion en X");
    Label lblY = new Label("Ubicacion en Y");
    FlowPane pane1 = new FlowPane();
    FlowPane pane2 = new FlowPane();
    FlowPane pane3 = new FlowPane();
    FlowPane pane4 = new FlowPane();
    FlowPane pane5 = new FlowPane();
    VBox root = new VBox();
    TextField textnombre = new TextField();
    TextField textcedula = new TextField();
    TextField textruta = new TextField();
    TextField textX = new TextField();
    TextField textY = new TextField();
    Stage agenteStage;

    public CreacionAgente(Agente a) {
        start(agenteStage);
    }
    
    private Parent CrearVentanaCreacionAgente(){
    pane1.getChildren().addAll(lblnombre,textnombre);
    pane2.getChildren().addAll(lblcedula,textcedula);
    pane3.getChildren().addAll(lblruta,textruta);
    pane4.getChildren().addAll(lblX,textX);
    pane5.getChildren().addAll(lblY,textY);
    
    root.getChildren().addAll(pane1,pane2,pane3,pane4,pane5);
    
    return root;
    }
        
    public void start(Stage Stage) {
    Stage = new Stage();
    Stage.setTitle("Creacion Agente");
    Stage.setScene(new Scene(CrearVentanaCreacionAgente()));
    Stage.show();
        
    }
    
    
    
        
    }
            
    
    
    
    
    
    

