/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mejoramientoproyecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class Agente implements Comparable{
    private String cedula;
    private String nombre;
    private String image_path;
    private Ubicacion ubicacion_actual;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public Ubicacion getUbicacion_actual() {
        return ubicacion_actual;
    }

    public void setUbicacion_actual(Ubicacion ubicacion_actual) {
        this.ubicacion_actual = ubicacion_actual;
    }

    public Agente(String cedula, String nombre, String image_path, Ubicacion ubicacion_actual) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.image_path = image_path;
        this.ubicacion_actual = ubicacion_actual;
    }

    @Override
    public int compareTo(Object o) {
        return this.nombre.compareTo(((Agente)o).nombre); 
    }
    
    static Set<Agente> cargarAgentesArchivo(String nombre_archivo) {
        Set<Agente> conjuntoAgentes = null;
        try{
            String agente;
            FileReader f = new FileReader(nombre_archivo);
            BufferedReader b = new BufferedReader(f);
            while((agente= b.readLine())!=null){
                String[] atributos = agente.split(",");
                Agente a = new Agente(atributos[1],atributos[0],atributos[2],new Ubicacion(Double.parseDouble(atributos[3]),Double.parseDouble(atributos[4])));
                conjuntoAgentes.add(a);
            }  
        }catch (ProcesarArchivosException e) {
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conjuntoAgentes;
          
    }

    
    /*@Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.cedula);
        hash = 97 * hash + Objects.hashCode(this.nombre);
        return Object.hash(nombre);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Agente other = (Agente) obj;
        if (!Objects.equals(this.cedula, other.cedula)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }*/
    
    @Override
    public boolean equals(Object obj) {
        if ((obj instanceof Agente) && (((Agente)obj).getCedula()==this.cedula) && (((Agente)obj).getNombre()==this.nombre)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.cedula);
        hash = 41 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    
}
