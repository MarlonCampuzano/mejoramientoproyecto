/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mejoramientoproyecto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author IKAROS
 */
public class ListadoVisitas {
    private Map<Agente,Set<Tienda>> visitas;

    public ListadoVisitas(Map<Agente, Set<Tienda>> visitas) {
        this.visitas = visitas;
    }
    
    public List<Tienda> calcularRuta(Agente a){
        ArrayList<Tienda> listadoTiendas = null;
        for(Agente agente: visitas.keySet()){
            if(agente.equals(a)){
                listadoTiendas = new ArrayList(visitas.get(a));
            }
        }
        Collections.sort(listadoTiendas, new Comparator(){
            @Override
            public int compare(Object t1 , Object t2) {
                Tienda t3 = (Tienda) t1;
                Tienda t4 = (Tienda) t2;
                int a1 = (int)Ubicacion.calcularDistancia(a.getUbicacion_actual(), t3.getUbicacion());
                int a2 = (int)Ubicacion.calcularDistancia(a.getUbicacion_actual(), t4.getUbicacion());
                return new Integer(a1).compareTo(new Integer(a2));
                
            }
        });
        
        return listadoTiendas;
        }
        
        
        
        
    }
    

